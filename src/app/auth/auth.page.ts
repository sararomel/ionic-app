import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionSheetController, LoadingController } from '@ionic/angular';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
isLoading =false;
isLogin = true;
  constructor(private authServices: AuthService, private router: Router, private loadingCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController ) { }

  ngOnInit() {
  }
onLogin(){
  this.isLoading= true ;
  this.authServices.login() ;
  this.loadingCtrl.create({keyboardClose:true , message:'logging in ......'}).then(loadingEl => {
    loadingEl.present() ;
    setTimeout(()=>{
      this.isLoading= false;
      loadingEl.dismiss() ;
      this.router.navigateByUrl('/places/tabs/discover');
    },1500) ;
  }) ;
}
onSubmit(form: NgForm){
if(!form.valid){
return;
}
const email=form.value.email;
const password=form.value.password;
console.log(email,password);
if(this.isLogin){
//send req login
}else{
  //send req signup
}
}
onSwitchAuthModel(){
this.isLogin = !this.isLogin ;
}
}
