import { Component, OnInit } from '@angular/core';
import { PlacesService } from '../places.service';
import { Place } from '../places.model';
import { IonItem, IonItemSliding } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {
  offers: Place[];
  constructor(private placesServices: PlacesService, private router: Router) { }

  ngOnInit() {
    this.offers = this.placesServices.places;
    console.log(this.offers);

  }
  onEdit(offerId: string, slidingItem: IonItemSliding) {
    slidingItem.close();
    this.router.navigate(['/', 'places', 'tabs', 'offers', 'edit', offerId]);
    console.log('Editing item ', offerId);
  }
}
