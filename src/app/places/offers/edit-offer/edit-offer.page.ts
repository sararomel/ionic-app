import { Component, OnInit } from '@angular/core';
import { Place } from '../../places.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit {
// place: Place[] ;
form: FormGroup;

  constructor() { }

  ngOnInit() {
    this.form=new FormGroup({
      title: new FormControl(null,{
        updateOn:'blur',
        validators:[Validators.required]
      }),
      description:new FormControl(null,{
        updateOn:'blur',
        validators:[Validators.required,Validators.maxLength(180)]
      }),
      price:new FormControl(null,{
        updateOn:'blur',
        validators:[Validators.required,Validators.min(1)]
      }),
      datefrom:new FormControl(null,{
        updateOn:'blur',
        validators:[Validators.required]
      }),
      dateto:new FormControl(null,{
        updateOn:'blur',
        validators:[Validators.required]
      }),
    });
  }
  onCreateoffer(){
    console.log(this.form);

  }
}
