/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';
import { Bookings } from './bookings.model';
@Injectable({
  providedIn: 'root'
})
export class BookingsService {
private _bookings: Bookings []=[
  {id: 'id 1',
  placeId: 'p1',
  placeTitle: 'Mahantan mansion',
  guestNumber: 2,
  userId : 'abc'
}

] ;
get bookings(){
return [...this._bookings] ;
}
  constructor() { }
}
