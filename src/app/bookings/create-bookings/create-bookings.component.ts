import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Place } from 'src/app/places/places.model';

@Component({
  selector: 'app-create-bookings',
  templateUrl: './create-bookings.component.html',
  styleUrls: ['./create-bookings.component.scss'],
})
export class CreateBookingsComponent implements OnInit {
@Input() selectedPlace: Place ;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}
onCancel(){
this.modalCtrl.dismiss(null , 'cancel') ;
}
onBookPlace(){
this.modalCtrl.dismiss({message: 'this is a dummy'}, 'confirm') ;
}

}
